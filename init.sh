#!/bin/bash
set -e

### Configuration ###

APP_DIR=/Users/huzefagadi/Desktop/OTAPOC
GIT_URL=git://bitbucket.org/infiswifthuzefa/otapoc
RESTART_ARGS=

# Uncomment and modify the following if you installed Passenger from tarball
#export PATH=/path-to-passenger/bin:$PATH


### Automation steps ###

set -x

# Pull latest code
if [[ -e $APP_DIR ]]; then
  cd $APP_DIR
  git pull
else
  git clone $GIT_URL $APP_DIR
  cd $APP_DIR
fi

# Install dependencies
npm install
#npm prune

# Restart app
forever restart $RESTART_ARGS $APP_DIR/index.js